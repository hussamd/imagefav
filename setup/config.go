package setup

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

// These enums represent the environment modes this application can run under
const (
	Development = "development"
	Staging     = "staging"
	Production  = "production"
)

// Config defines this application's config
type Config struct {
	DBHost        string `envconfig:"DB_HOST" required:"true"`
	DBPort        string `envconfig:"DB_PORT" default:"5432"`
	DBName        string `envconfig:"DB_NAME" required:"true"`
	DBUser        string `envconfig:"DB_USER" required:"true"`
	DBPassword    string `envconfig:"DB_PASSWORD" required:"true"`
	DBMaxOpenConn int    `envconfig:"DB_MAX_OPEN_CONN" default:"20"`
	DBMaxIdleConn int    `envconfig:"DB_MAX_IDLE_CONN" default:"5"`
	DBMaxLifeConn int    `envconfig:"DB_MAX_LIFE_CONN_MINS" default:"10"`

	UserMinLen int
	UserMaxLen int
	PassMinLen int
	PassMaxLen int
	NameMinLen int
	NameMaxLen int

	Environment string `envconfig:"ENVIRONMENT" default:"development"`

	GiphyURL      string `envconfig:"GIPHY_URL" default:"https://api.giphy.com"`
	GiphyPassword string `envconfig:"GIPHY_PASSWORD" required:"true"`

	HashKey string `envconfig:"SECRET_PASSWORD" required":true"`

	RedisURL           string `envconfig:"REDIS_URL" default:":9000"`
	SessionTimeoutSecs int    `envconfig:"SESSION_TIMEOUT_SECS" default:"28800"`

	URL  string `envconfig:"APP_URL" required:"true"`
	Port string `envconfig:"APP_PORT" default:"3000"`

	Error     error
	UserAgent string `envconfig:"USER_AGENT" default:"ImageFav Client 1.0"`

	Pagination struct {
		DefaultOffset int
		DefaultLimit  int
		MaxLimit      int
	}

	Initialized bool
}

// AppConfig holds environment variables/defaults
var AppConfig Config

// InitConfig will process environment variables and store them within the AppConfig package variable
func InitConfig() {
	if AppConfig.Initialized {
		return
	}

	err := envconfig.Process("", &AppConfig)
	if err != nil {
		log.Println("Could not load app config: ", err)
		AppConfig.Error = err
	}

	AppConfig.UserMinLen = 2
	AppConfig.UserMaxLen = 20
	AppConfig.PassMinLen = 8
	AppConfig.PassMaxLen = 100
	AppConfig.NameMinLen = 2
	AppConfig.NameMaxLen = 255

	AppConfig.Pagination.DefaultOffset = 0
	AppConfig.Pagination.DefaultLimit = 5
	AppConfig.Pagination.MaxLimit = 10

	AppConfig.Initialized = true
}
