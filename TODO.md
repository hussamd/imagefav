# TODO

---

* Giphy's API rate limits, catch the response headers and use them appropriately
* Cache individual gif results
* Allow user to reset their credentials
* Write a lot more tests
