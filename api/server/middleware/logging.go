package middleware

import (
	"log"
	"net/http"

	"bitbucket.org/hussamd/imagefav/api/types/handlers"
	"github.com/julienschmidt/httprouter"
)

// Log is a request middleware to ensure requests are logged
func Log(next handlers.Handle) handlers.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
		log.Printf("[%s] %s\n", r.Method, r.URL)
		return next(w, r, p)
	}
}
