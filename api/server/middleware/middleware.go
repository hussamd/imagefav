// Package middleware contains all route handlers that can be chained together to form an end route.
package middleware
