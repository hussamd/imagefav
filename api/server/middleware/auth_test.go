package middleware

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/hussamd/imagefav/api/server/session"
	mock "bitbucket.org/hussamd/imagefav/test/mock/session"
	"github.com/julienschmidt/httprouter"
)

func TestAuthMiddlewareTokenValidation(t *testing.T) {
	tests := []struct {
		name   string
		token  string
		cookie string
		ttl    int
		fail   bool
		sess   map[string]string
	}{
		{
			name:   "Should fail on missing X-Auth-Token header",
			token:  "",
			cookie: "",
			fail:   true,
		},
		{
			name:   "Should fail for non-existing session",
			token:  "",
			cookie: "from-cookie",
			fail:   true,
		},
		{
			name:   "Should fail for expired session",
			token:  "from-token",
			cookie: "",
			ttl:    1,
			fail:   true,
			sess: map[string]string{
				"id":       "abcd",
				"username": "hercules",
				"expires": strconv.FormatInt(
					time.Now().Add(time.Second*time.Duration(-1)).Unix(), 10,
				),
			},
		},
		{
			name:   "Should pass on a valid token",
			token:  "",
			cookie: "from-cookie",
			ttl:    1,
			fail:   false,
			sess: map[string]string{
				"id":       "abcd",
				"username": "hercules",
				"expires":  strconv.FormatInt(time.Now().Unix(), 10),
			},
		},
	}

	for _, test := range tests {
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "http://localhost:8080/search", nil)

		hasAuth := false
		if test.token != "" {
			hasAuth = true
			r.Header.Set("X-Auth-Token", test.token)
		}
		if test.cookie != "" {
			hasAuth = true
			r.Header.Set("Cookie", "session="+test.cookie)
		}
		noop := func(http.ResponseWriter, *http.Request, httprouter.Params) error {
			return nil
		}

		ttl := time.Second * time.Duration(1)
		sessionClient := session.NewClient
		session.NewClient = mock.NewTestClient(hasAuth && test.sess == nil, ttl, test.sess)
		defer func() { session.NewClient = sessionClient }()

		err := Auth(noop)(w, r, nil)
		if test.fail && err == nil {
			t.Fatalf(
				"TestAuthMiddlewareTokenValidation(%s) expected to fail but it did not",
				test.name,
			)
		}

		if !test.fail && err != nil {
			t.Fatalf(
				"TestAuthMiddlewareTokenValidation(%s) *did not* expect to fail but it did: %v",
				test.name, err,
			)
		}
	}
}
