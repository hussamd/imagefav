package middleware

import (
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/api/server/session"
	"bitbucket.org/hussamd/imagefav/api/types/handlers"
	"github.com/julienschmidt/httprouter"
)

const (
	headerAuth = "X-Auth-Token"
)

// Auth is a request middleware to ensure proper authentication is in place
// for incoming requests
func Auth(next handlers.Handle) handlers.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
		token := strings.TrimSpace(r.Header.Get(headerAuth))
		if token == "" {
			cookie, err := r.Cookie(cookieSessionName)
			if err != nil {
				if strings.Contains(err.Error(), "not present") {
					return errors.ErrMissingAuth
				}
				return err
			}

			if strings.TrimSpace(cookie.Value) == "" {
				return errors.ErrMissingAuth
			}
			token = strings.TrimSpace(cookie.Value)
		}

		sess, err := session.Get(token, true)
		if err != nil { // possible session expiration
			return err
		}

		if sess.Expired() {
			return errors.ErrSessionExpired
		}

		r.Header.Set("X-User-Name", sess.Username)
		r.Header.Set("X-Token", sess.ID)
		w.Header().Set("X-Session-Expires", strconv.FormatInt(sess.Expires, 10))
		return next(w, r, p)
	}
}
