package middleware

import (
	"net/http"
	"time"

	"bitbucket.org/hussamd/imagefav/api/types/handlers"
	"bitbucket.org/hussamd/imagefav/setup"
	"github.com/julienschmidt/httprouter"
)

const (
	cookieSessionName = "session"
)

// DeleteCookie instructs the browser to immediately expire the cookie
func DeleteCookie(next handlers.Handle) handlers.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
		err := next(w, r, p)
		if err == nil {
			sessionCookie := http.Cookie{
				Name:     cookieSessionName,
				Domain:   "",
				Path:     "/",
				Value:    "",
				Expires:  time.Now(),
				MaxAge:   0,
				Secure:   setup.AppConfig.Environment == setup.Production,
				HttpOnly: setup.AppConfig.Environment == setup.Production,
			}
			http.SetCookie(w, &sessionCookie)
		}
		return err
	}
}

// SetCookie is a middlware that will send back a Set-Cookie in the response
// header once request has successfully completed.
func SetCookie(next handlers.Handle) handlers.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) error {
		err := next(w, r, p)
		if err == nil {
			sessionCookie := http.Cookie{
				Name:     cookieSessionName,
				Domain:   "",
				Path:     "/",
				Value:    w.Header().Get("X-Token"),
				Expires:  time.Now().AddDate(0, 0, 1),
				Secure:   setup.AppConfig.Environment == setup.Production,
				HttpOnly: setup.AppConfig.Environment == setup.Production,
			}
			http.SetCookie(w, &sessionCookie)
		}
		return err
	}
}
