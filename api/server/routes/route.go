// Package routes contains all the defined routes for this application.
package routes

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/hussamd/imagefav/api/errors"
	apihandlers "bitbucket.org/hussamd/imagefav/api/server/handlers"
	"bitbucket.org/hussamd/imagefav/api/server/handlers/auth"
	"bitbucket.org/hussamd/imagefav/api/server/handlers/favorites"
	"bitbucket.org/hussamd/imagefav/api/server/handlers/health"
	"bitbucket.org/hussamd/imagefav/api/server/handlers/images"
	"bitbucket.org/hussamd/imagefav/api/server/handlers/tags"
	"bitbucket.org/hussamd/imagefav/api/server/middleware"
	"bitbucket.org/hussamd/imagefav/api/types/handlers"
	"github.com/julienschmidt/httprouter"
)

// sendError writes out the given error as a json string
func sendError(w http.ResponseWriter, e errors.HTTPError) {
	data := map[string]string{
		"status":      strconv.Itoa(e.Status()),
		"title":       http.StatusText(e.Status()),
		"description": e.Error(),
	}
	jsonResult, err := apihandlers.JSONMarshal(data)
	if err != nil {
		log.Printf("Failed encoding response to json: %v\n", err)
		apihandlers.Write(http.StatusInternalServerError, nil, w)
		return
	}

	apihandlers.Write(e.Status(), jsonResult, w)
}

// defaultHandler is a wrapper around httprouter.Handle that checks for any
// errors returned by the API and ensures that user facing errors are properly
// displayed, while all other errors are trapped by an Internal Server Error
func defaultHandler(h handlers.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		err := middleware.Log(h)(w, r, p)
		w.Header().Set("Content-Type", "application/json")
		if err != nil {
			switch e := err.(type) {
			case errors.HTTPError:
				log.Printf("HTTP (%d): %s", e.(errors.HTTPError).Status(), e)
				sendError(w, e)
			default:
				log.Printf("HTTP (503): %s", e.Error())
				sendError(w, errors.NewInternalServerError("Something went wrong"))
			}
		}
	}
}

// authHandler is used to wrap httprouter.Handle handlers with an
// authentication layer to ensure protected routes require proper authentication
func authHandler(h handlers.Handle) httprouter.Handle {
	return defaultHandler(middleware.Auth(h))
}

// Bootstrap initializes all the routes for this API
func Bootstrap() *httprouter.Router {
	r := httprouter.New()

	r.GET("/healthz", defaultHandler(health.StatusHandler))

	r.POST("/register", defaultHandler(auth.RegisterHandler))
	r.POST("/login", defaultHandler(auth.LoginHandler))
	r.POST("/login/web", defaultHandler(middleware.SetCookie(auth.LoginHandler)))
	r.POST("/logout", authHandler(auth.LogoutHandler))
	r.POST("/logout/web", authHandler(middleware.DeleteCookie(auth.LogoutHandler)))
	r.GET("/session", authHandler(auth.SessionHandler))

	r.GET("/search", defaultHandler(images.ListHandler))

	r.GET("/favorites", authHandler(favorites.ListHandler))
	r.POST("/favorites", authHandler(favorites.AddHandler))

	r.GET("/tags", authHandler(tags.ListHandler))
	r.POST("/favorites/:imageID/tags", authHandler(tags.AddHandler))
	r.DELETE("/favorites/:imageID/tags/:tagID", authHandler(tags.DeleteHandler))

	r.DELETE("/favorites/:imageID", authHandler(favorites.DeleteHandler))

	// This would preferrably be /favorites/query, but the route handling
	// in httprouter cannot distinguish between a parameter and a static value
	// Sad days ahead.
	r.POST("/query/favorites", authHandler(favorites.QueryHandler))

	return r
}
