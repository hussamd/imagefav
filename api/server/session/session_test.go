package session

import (
	"strconv"
	"testing"
	"time"

	mock "bitbucket.org/hussamd/imagefav/test/mock/session"
)

// Test sessions
var expiredSession = session{
	"id":       "abcd",
	"username": "hercules",
	"expires":  "-10",
}

var expiredUserSession = UserSession{
	ID:       "abcd",
	Username: "hercules",
	Expires:  int64(-10),
}

func newTimedSession(exp int64) UserSession {
	return UserSession{
		ID:       "abcd",
		Username: "hercules",
		Expires:  exp,
	}
}

// TestUserSessionExpiration tests whether a UserSession can detect a proper
// session expiration
func TestUserSessionExpiration(t *testing.T) {
	tests := []struct {
		name    string
		sess    UserSession
		expired bool
	}{
		{
			name:    "Should detect an expired",
			sess:    expiredUserSession,
			expired: true,
		},
		{
			name: "Should detect a non-expired session",
			sess: newTimedSession(
				time.Now().Add(time.Second * time.Duration(5)).Unix(),
			),
			expired: false,
		},
	}

	for _, test := range tests {
		if test.sess.Expired() != test.expired {
			t.Errorf(
				"TestUserSessionExpiration(%s): expected %t, got %t",
				test.name, test.expired, test.sess.Expired(),
			)
		}
	}
}

// TestSessionConversion tests that the proper conversion of a map type to
// a UserSession type without losing data
func TestSessionConversion(t *testing.T) {
	sess := expiredSession.ToUserSession(expiredSession["id"])
	expires, _ := strconv.ParseInt(expiredSession["expires"], 10, 64)
	if expiredSession["id"] != sess.ID ||
		expiredSession["username"] != sess.Username ||
		expires != sess.Expires {
		t.Errorf("TestSessionConversion expected a match but didn't")
	}
	/*
		"Should properly extend a user session"
	*/
}

func TestSessionExtension(t *testing.T) {
	ttl := time.Second * time.Duration(1)
	actualClient := NewClient // defined in session.go
	NewClient = mock.NewTestClient(false, ttl, nil)
	defer func() { NewClient = actualClient }() // restore package's client

	futureSecs := time.Now().Add(ttl).Unix()
	us := newTimedSession(futureSecs)
	if err := us.Extend(); err != nil {
		t.Errorf("TestSessionExtension: unexpected error: %v\n", err)
	}
	if us.Expires <= futureSecs { // Should be extended well beyond ttl
		t.Errorf("TestSessionExtension: failed, time = %d\n", us.Expires)
	}
}
