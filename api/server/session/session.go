// Package session provides session management capabilities.
package session

import (
	"bytes"
	"encoding/gob"
	"log"
	"strconv"
	"time"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/pkg/auth"
	"bitbucket.org/hussamd/imagefav/pkg/redis"
	"bitbucket.org/hussamd/imagefav/setup"
)

// UserSession holds information needed to make decisions about a user's session
type UserSession struct {
	ID       string
	Username string
	Expires  int64 // time.Unix
}

// session is an alias to a map of strings that is stored, byte encoded, into
// redis for session management
type session map[string]string

// timeoutInactive is the default inactivity timeout.
//
// This is not the total session timeout/cookie expiration timeout.
var timeoutInactive = time.Minute * 5

// ToUserSession converts the simple map sessino type into a UserSession struct
func (s session) ToUserSession(sID string) *UserSession {
	exp, err := strconv.ParseInt(s["expires"], 10, 64)
	if err != nil {
		log.Printf("Invalid user session expiration: %v\n", err)
		exp = -1
	}

	return &UserSession{
		ID:       sID,
		Username: s["username"],
		Expires:  exp,
	}
}

// NewClient points to a function that can return a store for session management
var NewClient = newRedisClient

// newRedisClient instantiates and returns a new redis client connection
func newRedisClient() redis.SimpleClient {
	r, err := redis.NewClient()
	if err != nil {
		log.Panicf("Session database unavailable: %v", err)
	}
	return r
}

// Get attempts to retrieve a UserSession by username.
//
// In most use cases, a session is being retrieved to validate its existence
// and timeout threshold. Therefore, it is possible to request an extension
// of the session while retrieving it as well.
//
// Pass a false value to `extend` if you wish to only inspect the data and
// not alter the session in any way.
//
// A session can expire while the UserSession object is still instantiated.
// Always check for its validity before commiting it back to the store. A
// convenience method is provided with `Expired()` for this reason.
func Get(sID string, extend bool) (*UserSession, error) {
	r := NewClient()

	// Session expired or never existed
	sess := map[string]string{}
	if err := r.GetUnmarshaled(sID, &sess); err != nil {
		return nil, errors.ErrInvalidSession
	}

	us := session(sess).ToUserSession(sID)
	if us.Expired() {
		return nil, errors.ErrSessionExpired
	}

	if extend {
		// Slide the expiration window until max time every time it's accessed
		us.Extend()
	}
	return us, nil
}

// Terminate removes the user session from the underlying store.
//
// It is not an error to terminate a session that does not exist.
func (u *UserSession) Terminate() error {
	r := NewClient()
	if err := r.Del(u.ID); err != nil {
		return err
	}

	return nil
}

// Extend the user's session by the default inactivity duration
func (u *UserSession) Extend() error {
	r := NewClient()

	ttl, err := r.TTL(u.ID) // ensure that the record exists
	if err != nil {
		return err
	}

	now := time.Now().Unix() // seconds
	timeout := int64(timeoutInactive.Seconds())
	untilTimeout := time.Until(time.Unix(now+timeout, 0))
	if ttl.Minutes() < untilTimeout.Minutes() {
		if err := r.Expire(u.ID, untilTimeout); err != nil {
			return err
		}
		u.Expires = time.Unix(now+timeout, 0).Unix()
	}

	return nil
}

// Expired determines if the instantiated user session object's expiration
// window has been reached.
func (u *UserSession) Expired() bool {
	return u.Expires < time.Now().Unix()
}

// New generates a user session and persists it to the underlying store
func New(id, username string, ttl time.Duration) (*UserSession, error) {
	r := NewClient()

	exp := time.Now().Add(ttl).Unix() // utc
	log.Printf(
		"[%s]: New session will expire: %s\n",
		username,
		time.Unix(exp, 0).Format(time.RFC3339),
	)
	sess := session{
		"id":       id,
		"username": username,
		"expires":  strconv.FormatInt(exp, 10),
	}
	bs, err := toBytes(sess)
	if err != nil {
		return nil, errors.NewInternalServerError(err.Error())
	}

	appCfg := setup.AppConfig
	sID := auth.NewToken([]byte(appCfg.HashKey), bs)
	r.Set(sID, sess, timeoutInactive) // expire due to inactivity

	return sess.ToUserSession(sID), nil
}

func toBytes(data interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
