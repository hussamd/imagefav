package favorites

import (
	"bitbucket.org/hussamd/imagefav/api/types/pagination"
)

// ListResult contains information from listing a user's favorites
type ListResult struct {
	Images []Favorite       `json:"favorites"`
	Links  pagination.Links `json:"_links"`
	Count  int              `json:"count"`
	Total  int              `json:"total"`
}

// Favorite holds an image IDs that is favorited by a user
type Favorite struct {
	Image string   `json:"image"`
	URL   string   `json:"url"`
	Tags  []string `json:"tags"`
}

// Query holds a list of image IDs that are potentially favorited by a user
type Query struct {
	Images []string `validate:"gt=0,lte=100,dive,gt=0,lt=25,required"`
}

// QueryResult holds the result of querying for a list of image IDs by a user
type QueryResult struct {
	Found    []Favorite `json:"favorites"`
	NotFound []string   `json:"not_found"`
}
