// Package favorites contains route handlers for managing favorited images by a user.
package favorites

import (
	"fmt"
	"log"
	"net/http"
	gourl "net/url"
	"strings"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/api/model/favorite"
	"bitbucket.org/hussamd/imagefav/api/model/user"
	"bitbucket.org/hussamd/imagefav/api/server/handlers"
	"bitbucket.org/hussamd/imagefav/api/types/pagination"
	"bitbucket.org/hussamd/imagefav/internal/url"
	"bitbucket.org/hussamd/imagefav/pkg/giphy"
	"bitbucket.org/hussamd/imagefav/setup"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/go-playground/validator.v9"
)

const (
	paramTags = "tags"
)

func tagsFromParams(params gourl.Values) []string {
	tags := make([]string, 0)
	for _, t := range params[paramTags] {
		sanitized := strings.TrimSpace(t)
		if sanitized != "" {
			tags = append(tags, t)
		}
	}

	return tags
}

// ListHandler is the route handler for `GET /favorites`
func ListHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	// Params
	qp := r.URL.Query()
	p, err := pagination.FromParams(qp)
	if err != nil {
		return err
	}
	tags := tagsFromParams(qp)

	// Lookup
	u, err := user.FindByUsername(r.Context(), r.Header.Get("X-User-Name"))
	if err != nil {
		return err
	}

	favs, err := favorite.List(r.Context(), u.ID, tags, p.Offset, p.Limit)
	if err != nil {
		return err
	}

	total, err := favorite.Count(r.Context(), u.ID)
	if err != nil {
		return err
	}

	images := make([]Favorite, 0)
	for i, f := range favs {
		images = append(images, Favorite(f))
		if images[i].Tags == nil {
			images[i].Tags = []string{}
		}
	}

	// Render
	absURL := url.AbsURL(setup.AppConfig.URL, "/favorites")
	result := ListResult{
		Images: images,
		Links:  pagination.NewLinks(absURL, total, p, qp),
		Count:  len(images),
		Total:  total,
	}

	jsonResult, err := handlers.JSONMarshal(result)
	if err != nil {
		return errors.NewMarshalingError(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err = fmt.Fprint(w, string(jsonResult)); err != nil {
		return errors.NewResponseError(err)
	}
	return nil
}

// AddHandler is the route handler for `POST /favorites`
func AddHandler(
	w http.ResponseWriter, r *http.Request, p httprouter.Params,
) error {
	// Read
	bs, err := handlers.Read(r)
	if err != nil {
		return err
	}

	// Parse
	fav := Favorite{}
	if err := handlers.Parse(bs, &fav); err != nil {
		return err
	}

	// Validate
	if strings.TrimSpace(fav.Image) == "" {
		return errors.ErrInvalidImage
	}

	// Ensure image exists
	img, err := giphy.Get(r.Context(), fav.Image)
	if err != nil {
		switch err.(type) {
		case errors.NotFound:
			return errors.ErrImageNotFound
		default:
			return err
		}
	}

	// Execute
	username := r.Header.Get("X-User-Name")
	u, err := user.FindByUsername(r.Context(), username)
	if err != nil {
		return err
	}
	rec, gerr := favorite.Get(r.Context(), u.ID, fav.Image)
	if gerr != nil {
		return gerr
	}

	if (rec == favorite.Record{}) { // not previously favorited
		if err := favorite.Save(r.Context(), favorite.Image{
			Username: username,
			ImageID:  fav.Image,
			URL:      img.Gif.Links.Original.URL,
		}); err != nil {
			return err
		}
	}
	return nil
}

// DeleteHandler is the route handler for `DELETE /favorites/:id`
func DeleteHandler(
	w http.ResponseWriter, r *http.Request, p httprouter.Params,
) error {
	// Parse
	imageID := p.ByName("imageID")

	// Validate
	if strings.TrimSpace(imageID) == "" {
		return errors.ErrInvalidImage
	}

	if err := favorite.Delete(
		r.Context(), r.Header.Get("X-User-Name"), imageID,
	); err != nil {
		return err
	}

	return nil
}

// QueryHandler is the route handler for `POST /favorites/query`
func QueryHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	// Read
	bs, err := handlers.Read(r)
	if err != nil {
		return err
	}

	// Parse
	var q struct {
		Query `json:"images"`
	}

	if err := handlers.Parse(bs, &q.Query); err != nil {
		return err
	}

	// Validate
	if len(q.Images) == 0 {
		return errors.ErrInvalidQuery
	}

	validate := validator.New()
	if err := validate.Struct(q); err != nil {
		log.Printf("Invalid query: %v\n", err)
		return errors.ErrInvalidQuery
	}

	found, err := favorite.Query(
		r.Context(), r.Header.Get("X-User-Name"), q.Images,
	)
	if err != nil {
		return err
	}

	result := QueryResult{[]Favorite{}, []string{}}
	for _, i := range q.Images {
		if tags, ok := found[i]; ok {
			result.Found = append(result.Found, Favorite{
				Image: i,
				Tags:  tags,
			})
		} else {
			result.NotFound = append(result.NotFound, i)
		}
	}

	jsonResult, err := handlers.JSONMarshal(result)
	if err != nil {
		return errors.NewMarshalingError(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err = fmt.Fprint(w, string(jsonResult)); err != nil {
		return errors.NewResponseError(err)
	}
	return nil
}
