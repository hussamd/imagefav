package tags

// UserTags contains all tags belonging to a user and their frequency
type UserTags struct {
	Tag   string `json:"tag"`
	Count int    `json:"count"`
}

// ListResult is a container for UserTags
type ListResult struct {
	Tags []UserTags `json:"tags"`
}

// Tag is an attribute of a user favorited image
type Tag struct {
	Value string `json:"tag" validate:"gt=2,lte=100,required"`
}
