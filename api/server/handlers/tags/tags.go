// Package tags contains all the handlers for managing tags for favorited images.
package tags

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/api/model/favorite"
	"bitbucket.org/hussamd/imagefav/api/model/tag"
	"bitbucket.org/hussamd/imagefav/api/model/user"
	"bitbucket.org/hussamd/imagefav/api/server/handlers"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/go-playground/validator.v9"
)

// ListHandler is the route handler for `GET /tags`
func ListHandler(
	w http.ResponseWriter, r *http.Request, p httprouter.Params,
) error {
	// Lookup
	u, err := user.FindByUsername(r.Context(), r.Header.Get("X-User-Name"))
	if err != nil {
		return err
	}

	// Execute
	tags, err := tag.List(r.Context(), u.ID)
	if err != nil {
		return err
	}

	userTags := make([]UserTags, 0)
	for tag, count := range tags {
		userTags = append(userTags, UserTags{
			Tag:   tag,
			Count: count,
		})
	}

	// Render
	result := ListResult{
		Tags: userTags,
	}

	jsonResult, err := handlers.JSONMarshal(result)
	if err != nil {
		return errors.NewMarshalingError(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err = fmt.Fprint(w, string(jsonResult)); err != nil {
		return errors.NewResponseError(err)
	}
	return nil
}

// AddHandler is the route handler for `POST /favorites/:imageID/tags`
func AddHandler(
	w http.ResponseWriter, r *http.Request, p httprouter.Params,
) error {
	// Lookup
	u, err := user.FindByUsername(r.Context(), r.Header.Get("X-User-Name"))
	if err != nil {
		return err
	}

	// Parse
	imageID := p.ByName("imageID")

	// Validate
	if strings.TrimSpace(imageID) == "" {
		return errors.ErrInvalidImage
	}

	t := Tag{}
	bs, err := handlers.Read(r)
	if err != nil {
		return err
	}
	if err := handlers.Parse(bs, &t); err != nil {
		return err
	}
	validate := validator.New()
	if err := validate.Struct(t); err != nil {
		log.Printf("Invalid tag: %v\n", err)
		return errors.ErrInvalidTag
	}

	image, err := favorite.Get(r.Context(), u.ID, imageID)
	if err != nil {
		return err
	}
	if (image == favorite.Record{}) {
		return errors.ErrImageNotFound
	}

	// Execute
	if err := tag.Add(r.Context(), t.Value, u.ID, imageID); err != nil {
		return err
	}

	return nil
}

// DeleteHandler is the route handler for `DELETE /favorites/:imageID/tags/:tagID`
func DeleteHandler(
	w http.ResponseWriter, r *http.Request, p httprouter.Params,
) error {
	// Lookup
	u, err := user.FindByUsername(r.Context(), r.Header.Get("X-User-Name"))
	if err != nil {
		return err
	}

	// Parse
	imageID := p.ByName("imageID")
	tagID := p.ByName("tagID")

	// Validate
	if strings.TrimSpace(imageID) == "" {
		return errors.ErrInvalidImage
	}
	if strings.TrimSpace(tagID) == "" {
		return errors.ErrInvalidTag
	}

	// Execute
	if err := tag.Delete(r.Context(), u.ID, imageID, tagID); err != nil {
		return err
	}

	return nil
}
