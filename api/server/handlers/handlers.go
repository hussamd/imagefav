// Package handlers contains all the handlers implemented for each route.
package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/hussamd/imagefav/api/errors"
)

// DefaultContentType is the default content type supported by this service
const DefaultContentType = "application/json"

// NewContext returns a new context for use by all handlers.
//
// Handlers can use this context for propagating request IDs for tracing and to
// cancel ongoing requests (e.g. for abandoned connections)
func NewContext() (context.Context, context.CancelFunc) {
	return context.WithCancel(context.Background())
}

// JSONMarshal is a helper method to prevent escapting of <, >, and &
//
// https://stackoverflow.com/questions/28595664/how-to-stop-json-marshal-from-escaping-and
func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	enc := json.NewEncoder(buffer)
	enc.SetEscapeHTML(false)
	err := enc.Encode(t)
	return buffer.Bytes(), err
}

// Read takes in the request body expecting data to actually be there.
//
// Do not call this if you are not expecting any data in your requests.
func Read(r *http.Request) ([]byte, error) {
	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Failed reading request body: %v\n", err)
		return nil, err
	}
	if bs == nil || len(bs) == 0 {
		return nil, errors.ErrEmptyBody
	}

	return bs, nil
}

// Parse unmarshals the given byte data into dest
func Parse(data []byte, dest interface{}) error {
	if err := json.Unmarshal(data, dest); err != nil {
		log.Printf("Failed to parse request body: %v\n", err)
		return errors.ErrInvalidBody
	}
	return nil
}

// Write sends the provided data to the http writer object
func Write(status int, data []byte, w http.ResponseWriter) error {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", DefaultContentType)

	out := string(data)
	if _, err := fmt.Fprint(w, out); err != nil {
		return err
	}
	return nil
}
