// Package health contains a status route for checking the health of the application.
package health

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// StatusHandler is the route handler for `GET /healthz`
func StatusHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	log.Println("Pong")
	return nil
}
