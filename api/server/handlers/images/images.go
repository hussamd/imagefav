// Package images contains all the handlers for managing searches for an image.
package images

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/api/server/handlers"
	"bitbucket.org/hussamd/imagefav/api/types/pagination"
	_url "bitbucket.org/hussamd/imagefav/internal/url"
	"bitbucket.org/hussamd/imagefav/pkg/giphy"
	"bitbucket.org/hussamd/imagefav/setup"
	"github.com/julienschmidt/httprouter"
)

// SearchResult contains information from performing a search query
type SearchResult struct {
	Gifs  []giphy.Gif      `json:"gifs"`
	Links pagination.Links `json:"_links"`
	Count int              `json:"count"`
	Total int              `json:"total"`
}

// ListHandler is the route handler for `GET /search?q=`
func ListHandler(
	w http.ResponseWriter, r *http.Request, params httprouter.Params,
) error {
	// Search params
	qp := r.URL.Query()
	q := strings.TrimSpace(qp.Get("q"))
	if q == "" {
		return errors.ErrInvalidSearchParam
	}

	p, err := pagination.FromParams(qp)
	if err != nil {
		return err
	}

	// Execution
	ctx, cancel := handlers.NewContext()
	defer cancel()

	res, err := giphy.Search(ctx, q, p)
	if err != nil {
		return err
	}

	absURL := _url.AbsURL(setup.AppConfig.URL, "/search")
	result := SearchResult{
		Gifs:  giphy.TransformResults(res),
		Links: pagination.NewLinks(absURL, res.Pagination.Total, p, qp),
		Count: len(res.Gifs),
		Total: res.Pagination.Total,
	}

	jsonResult, err := handlers.JSONMarshal(result)
	if err != nil {
		return errors.NewMarshalingError(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err = fmt.Fprint(w, string(jsonResult)); err != nil {
		return errors.NewResponseError(err)
	}

	return nil
}
