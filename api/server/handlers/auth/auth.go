// Package auth contains handlers for authentication and session management.
package auth

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/api/model/user"
	"bitbucket.org/hussamd/imagefav/api/server/handlers"
	"bitbucket.org/hussamd/imagefav/api/server/session"
	"bitbucket.org/hussamd/imagefav/pkg/auth"
	"bitbucket.org/hussamd/imagefav/setup"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/go-playground/validator.v9"
)

var (
	// ErrEmpty is used when a field requiring data has no data
	ErrEmpty = fmt.Errorf("empty")
	// ErrTooLong is used when a field exceeds a max length for its data type
	ErrTooLong = fmt.Errorf("too long")
	// ErrTooShort is used when a field does not meet a min length for its data type
	ErrTooShort = fmt.Errorf("too short")
)

// Credentials holds a username and password together to pass around
type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Registration holds all the data needed to createa  new user
type Registration struct {
	Username string `json:"username" validate:"min=2,max=20,ascii,required"`
	Password string `json:"password" validate:"min=8,max=100,required"`
	Email    string `json:"email" validate:"email,max=255,required"`
	Name     string `json:"name" validate:"min=2,max=255,required"`
}

func badField(field string, min, max int) error {
	switch s := strings.TrimSpace(field); {
	case len(s) == 0:
		return ErrEmpty
	case len(s) < min:
		return ErrTooShort
	case len(s) > max:
		return ErrTooLong
	}
	return nil
}

// Validate ensures the data stored in Validation meets the schema defined
// in the struct itself
func (r Registration) Validate() error {
	appCfg := setup.AppConfig
	userMin, userMax := appCfg.UserMinLen, appCfg.UserMaxLen
	if err := badField(r.Username, userMin, userMax); err != nil {
		return errors.NewFieldError("username", err)
	}

	passMin, passMax := appCfg.PassMinLen, appCfg.PassMaxLen
	if err := badField(r.Password, passMin, passMax); err != nil {
		return errors.NewFieldError("password", err)
	}

	nameMin, nameMax := appCfg.NameMinLen, appCfg.NameMaxLen
	if err := badField(r.Name, nameMin, nameMax); err != nil {
		switch err {
		case ErrEmpty:
			return errors.ErrNameless
		case ErrTooShort:
			return errors.ErrNameTooShort
		case ErrTooLong:
			return errors.ErrNameTooLong
		default:
			return err
		}
	}

	validate := validator.New()
	if err := validate.Struct(r); err != nil {
		if strings.Contains(err.Error(), "Registration.Email") {
			return errors.ErrInvalidEmail
		}

		log.Printf("Invalid registration: %v\n", err)
		return errors.ErrInvalidRegistration
	}

	return nil
}

// ToUser converts a registration to a User type that matches a record in the
// database.
//
// Most importantly, it converts the plaintext password to a one way hash
// which makes a user object safe to pass around.
func (r Registration) ToUser() (*user.User, error) {
	p := auth.NewPassword([]byte(r.Password), nil)
	hashPass, err := p.NewHash()
	if err != nil {
		return nil, err
	}

	return &user.User{
		Username: r.Username,
		PwHash:   hashPass,
		Name:     r.Name,
		Email:    r.Email,
	}, nil
}

// SessionHandler is the route handler for `GET /session`
func SessionHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	if _, err := getSession(r); err != nil {
		return err
	}
	return nil
}

// RegisterHandler is the route handler for `POST /register`
func RegisterHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	// Read
	bs, err := handlers.Read(r)
	if err != nil {
		return err
	}

	// Parse
	reg := Registration{}
	if err := handlers.Parse(bs, &reg); err != nil {
		return err
	}

	// Validate
	if err := reg.Validate(); err != nil {
		return err
	}

	u, err := reg.ToUser()
	if err != nil {
		return err
	}

	if err := user.Save(r.Context(), *u); err != nil {
		switch err {
		case user.ErrDuplicateRecord:
			return errors.ErrUsernameTaken
		}
		return err
	}

	w.WriteHeader(http.StatusCreated)
	return nil
}

// LogoutHandler is the route handler for `POST /logout`
func LogoutHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	if err := delSession(r); err != nil {
		return err
	}
	return nil
}

// LoginHandler is the route handler for `POST /login`
func LoginHandler(
	w http.ResponseWriter, r *http.Request, _ httprouter.Params,
) error {
	// Read
	bs, err := handlers.Read(r)
	if err != nil {
		return err
	}

	// Parse
	creds := Credentials{}
	if err := handlers.Parse(bs, &creds); err != nil {
		return err
	}

	// Validate
	if creds.Username == "" || creds.Password == "" {
		return errors.ErrInvalidAuth
	}

	u, err := user.FindByUsername(r.Context(), creds.Username)
	if err != nil {
		switch err {
		case user.ErrNotFound:
			return errors.ErrInvalidAuth
		default:
			return fmt.Errorf("Failed to locate user for auth: %v", err)
		}
	}

	p := auth.NewPassword(nil, []byte(u.PwHash))
	matches, err := p.Compare(creds.Password)
	if err != nil {
		return fmt.Errorf("Unable to verify user password: %v", err)
	}

	if !matches {
		return errors.ErrFailedAuth
	}

	token, err := newSession(u)
	if err != nil {
		return nil
	}

	w.Header().Set("X-Token", token)
	return nil
}

// getSession attempts to retrieve an existing session for the given username
func getSession(r *http.Request) (*session.UserSession, error) {
	return session.Get(r.Header.Get("X-Token"), false)
}

// deleteSession terminates a user's session
func delSession(r *http.Request) error {
	userSession, err := getSession(r)
	if err != nil {
		return err
	}
	return userSession.Terminate()
}

// newSession creates a new user session for the given user
func newSession(u *user.User) (string, error) {
	appCfg := setup.AppConfig

	userSession, err := session.New(
		u.ID, u.Username, time.Second*time.Duration(appCfg.SessionTimeoutSecs),
	)
	if err != nil {
		return "", err
	}

	return userSession.ID, nil
}
