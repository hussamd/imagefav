package tag

import (
	"context"

	"bitbucket.org/hussamd/imagefav/internal/db"
)

// UserTag represents a single tag and its frequency belonging to a user
type UserTag struct {
	Tag   string
	Count int
}

// Delete removes a tag from the list of tags on a user's favorited image.
func Delete(ctx context.Context, userID, imageID, tag string) error {
	const stmt = `
	UPDATE user_favorites
	   SET tags = array_remove(tags, $1)
	 WHERE user_id = $2 AND image_id = $3
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return nil
	}
	defer conn.Close()

	if _, err := conn.ExecContext(ctx, stmt, tag, userID, imageID); err != nil {
		return err
	}

	return nil
}

// Add adds a new tag to the given user's favorited image.
//
// If the tag already exists, it is not added again.
func Add(ctx context.Context, tag, userID, imageID string) error {
	const stmt = `
	UPDATE user_favorites
	   SET tags = array_append(tags, $1)
	 WHERE user_id = $2 AND image_id = $3
	   AND NOT tags @> ARRAY[$1]::varchar[]
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return nil
	}
	defer conn.Close()

	if _, err := conn.ExecContext(ctx, stmt, tag, userID, imageID); err != nil {
		return err
	}

	return nil
}

// List returns all tags belonging to the given user
func List(ctx context.Context, userID string) (map[string]int, error) {
	const stmt = `
	WITH cte AS (SELECT unnest(tags) AS tag FROM user_favorites
							 WHERE user_id = $1)
	SELECT tag, count(tag) FROM cte GROUP BY tag
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	rows, err := conn.QueryContext(ctx, stmt, userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	tags := make(map[string]int)
	for rows.Next() {
		var t UserTag
		if err := rows.Scan(&t.Tag, &t.Count); err != nil {
			return nil, err
		}
		tags[t.Tag] = t.Count
	}

	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return tags, nil
}
