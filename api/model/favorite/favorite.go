package favorite

import (
	"context"
	"database/sql"
	"fmt"

	"bitbucket.org/hussamd/imagefav/internal/db"
	"github.com/lib/pq"
)

// Record represents a single user favorite record
type Record struct {
	UserID  string
	ImageID string
	URL     string
}

// Image represents an image favorited by a user
type Image struct {
	Username string
	ImageID  string
	URL      string
}

// Favorite is a single favorite record in the database
type Favorite struct {
	Image string
	URL   string
	Tags  []string
}

// Favorites represents a list of favorited images by a user
type Favorites []Favorite

// Query searches for the provided image ids by the given user and returns
// only those images that are found for the user
func Query(ctx context.Context, userID string, images []string) (map[string][]string, error) {
	const stmt = `
	SELECT image_id, tags
	  FROM user_favorites
	 WHERE image_id = ANY($1)
	   AND user_id = (SELECT id FROM users WHERE username = $2)
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	rows, err := conn.QueryContext(
		ctx, stmt, pq.Array(images), userID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	found := make(map[string][]string)
	for rows.Next() {
		var imageID string
		var tags = make([]string, 0)
		if err := rows.Scan(&imageID, pq.Array(&tags)); err != nil {
			return nil, err
		}
		if len(tags) > 0 {
			found[imageID] = tags
		} else {
			found[imageID] = []string{}
		}
	}

	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return found, nil
}

// Count returns the total number of favorites belonging to user
func Count(ctx context.Context, userID string) (int, error) {
	const stmt = `SELECT COUNT(image_id) FROM user_favorites WHERE user_id = $1`

	conn, err := db.Connect(ctx)
	if err != nil {
		return -1, err
	}
	defer conn.Close()

	row := conn.QueryRowContext(ctx, stmt, userID)

	var count int
	if err := row.Scan(&count); err != nil {
		switch err {
		case sql.ErrNoRows:
			return 0, nil
		default:
			return -1, err
		}
	}

	return count, nil
}

// Delete removes a favorite image record for the given user
func Delete(ctx context.Context, userID, imageID string) error {
	const stmt = `
	DELETE FROM user_favorites
	 WHERE user_id = (SELECT id FROM users WHERE username = $1)
				 AND image_id = $2
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return nil
	}
	defer conn.Close()

	if _, err := conn.ExecContext(ctx, stmt, userID, imageID); err != nil {
		return err
	}

	return nil
}

// Get returns a single favorite record by an image ID
func Get(ctx context.Context, userID, imageID string) (Record, error) {
	const stmt = `
	SELECT user_id, image_id, url
	  FROM user_favorites
	 WHERE user_id = $1 AND image_id = $2
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return Record{}, err
	}
	defer conn.Close()

	row := conn.QueryRowContext(ctx, stmt, userID, imageID)

	var r Record
	if err := row.Scan(&r.UserID, &r.ImageID, &r.URL); err != nil {
		switch err {
		case sql.ErrNoRows:
			return r, nil
		default:
			return r, err
		}
	}

	return r, nil
}

// List returns favorites for the given user, potentially filtered by tags,
// in the range specified
func List(
	ctx context.Context, userID string, tags []string, offset, limit int,
) (Favorites, error) {
	stmt := `
	SELECT image_id, url, tags
	  FROM user_favorites
	 WHERE user_id = $1 %s
ORDER BY created_at DESC
	OFFSET $2 LIMIT $3
	`

	if len(tags) > 0 {
		stmt = fmt.Sprintf(stmt, "AND tags @> $4")
	} else {
		stmt = fmt.Sprintf(stmt, "")
	}

	conn, err := db.Connect(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	var rows *sql.Rows
	if len(tags) > 0 {
		rows, err = conn.QueryContext(ctx, stmt, userID, offset, limit, pq.Array(tags))
	} else {
		rows, err = conn.QueryContext(ctx, stmt, userID, offset, limit)
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	favs := make(Favorites, 0)
	for rows.Next() {
		var fav Favorite
		if err := rows.Scan(&fav.Image, &fav.URL, pq.Array(&fav.Tags)); err != nil {
			return nil, err
		}
		favs = append(favs, fav)
	}

	// Check for errors from iterating over rows.
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return favs, nil
}

// Save idempotently commits a favorite record into the user favorites table
func Save(ctx context.Context, i Image) error {
	const stmt = `
INSERT INTO user_favorites(user_id, image_id, url)
     SELECT (SELECT id FROM users WHERE username = $1), CAST($2 as varchar), CAST($3 as varchar)
  WHERE NOT EXISTS (
		SELECT image_id FROM user_favorites WHERE user_id = (
			SELECT id FROM users WHERE username = $1
		) AND image_id = $2
	)
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.ExecContext(ctx, stmt, i.Username, i.ImageID, i.URL)
	if err != nil {
		return err
	}

	return nil
}
