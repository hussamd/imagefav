package user

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"strings"

	"bitbucket.org/hussamd/imagefav/internal/db"
)

var (
	// ErrCreate is used when user creation fails (e.g. duplicate username)
	ErrCreate = errors.New("Failed to create a new user record")
	// ErrNotFound is used when a user does not exist in our records
	ErrNotFound = errors.New("User not found")
	// ErrTooManyRecords is used when the search criteria provided matches too many records
	ErrTooManyRecords = errors.New("Too many records matched your criteria")
	// ErrDuplicateRecord is used when a unique key violation is made (e.g race
	// condition when adding the same tag from two different sources for the
	// same image)
	ErrDuplicateRecord = errors.New("One or more values violate a unique constraint")
)

// User represents a single record in the users table
type User struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Username string `json:"usenrame"`
	PwHash   string `json:"-"`
	Email    string `json:"email"`
}

// Save commits the given user object into the database
func Save(ctx context.Context, u User) error {
	const stmt = `
	INSERT INTO users (username, password, name, email)
	     VALUES ($1, $2, $3, $4)
	`

	conn, err := db.Connect(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	res, err := conn.ExecContext(ctx, stmt, u.Username, u.PwHash, u.Name, u.Email)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key") {
			return ErrDuplicateRecord
		}
		return err
	}

	count, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if count != 1 {
		log.Printf("Failed to create a new user record: %v\n", err)
		return ErrCreate
	}

	return nil
}

// FindByUsername locates an existing user by their username
func FindByUsername(ctx context.Context, username string) (*User, error) {
	const stmt = `
	SELECT id, username, password, name, email
	  FROM users
	 WHERE username = $1
	`
	user, err := fetchRecord(ctx, stmt, username)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// FindByID locates an existing user by their ID
func FindByID(ctx context.Context, id string) (*User, error) {
	const stmt = `
	SELECT id, username, password, name, email
	  FROM users
	 WHERE id = $1
	`
	user, err := fetchRecord(ctx, stmt, id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func fetchRecord(
	ctx context.Context, stmt string, args ...interface{},
) (*User, error) {
	conn, err := db.Connect(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	rec := new(User)
	row := conn.QueryRowContext(ctx, stmt, args...)
	err = row.Scan(&rec.ID, &rec.Username, &rec.PwHash, &rec.Name, &rec.Email)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return nil, ErrNotFound
		default:
			return nil, err
		}
	}

	return rec, nil
}
