// Package clients implements a base HTTP client for talking to external APIs.
package clients

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"time"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/internal/url"
)

// BaseClient is used to construct all outbound HTTP clients for various services.
//
// It is best to instantiate this type using the convenience method provided.
type BaseClient struct {
	HTTPClient *http.Client
	Auth       AuthProvider
	Host       string
}

// ExpectedState is a collection of status codes to be used in conjunction
// with HTTPDo for resolving responses
type ExpectedState []int

// Expect creates a new ExpectedState out of the given list of status codes
func Expect(states ...int) ExpectedState {
	expected := make(ExpectedState, len(states))
	for i := range states {
		expected[i] = states[i]
	}

	return expected
}

// Has determines if the given status is in the list of our expected statuses
func (e *ExpectedState) Has(status int) bool {
	for _, s := range *e {
		if s == status {
			return true
		}
	}
	return false
}

func prepBody(body []byte) io.Reader {
	if body == nil {
		return nil
	}
	return bytes.NewBuffer(body)
}

// BuildRequest is a convenience method to return a prepared request object for
// use with client.Do()
func (b BaseClient) BuildRequest(
	ctx context.Context, method, uri string, body []byte, params map[string]string,
	headers map[string]string,
) (*http.Request, error) {
	req, err := http.NewRequest(method, url.AbsURL(b.Host, uri), prepBody(body))
	if err != nil {
		return nil, err
	}

	if method == http.MethodGet {
		q := req.URL.Query()
		for k, v := range params {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	for k, v := range headers {
		req.Header.Set(k, v)
	}

	b.Auth.SetAuth(req)
	req = req.WithContext(ctx)

	return req, nil
}

// HTTPDo is a convenience method for calling client.Do but with the provided
// context and added logic to error on unwanted status codes
func (b BaseClient) HTTPDo(
	req *http.Request, expect ExpectedState, f func(io.ReadCloser) error,
) (*http.Response, error) {
	resp, err := b.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if expect.Has(resp.StatusCode) {
		if err := unmarshalResp(req, resp, f); err != nil {
			return resp, err
		}
		return resp, nil
	}

	bs, _ := ioutil.ReadAll(resp.Body)
	switch resp.StatusCode {
	case http.StatusNotFound:
		return resp, errors.NewNotFoundError(fmt.Sprintf("Not found: %s", bs))
	case http.StatusBadRequest:
		return resp, errors.NewBadRequestError(fmt.Sprintf("Bad request: %s", bs))
	default:
		return resp, fmt.Errorf("Request failed (HTTP %d): %s", resp.StatusCode, bs)
	}
}

func unmarshalResp(
	req *http.Request, resp *http.Response, f func(io.ReadCloser) error,
) error {
	if resp.StatusCode == 204 {
		return nil
	}

	if req.Method != http.MethodHead && req.Method != http.MethodDelete {
		if err := f(resp.Body); err != nil {
			return err
		}
	}

	return nil
}

func newHTTPClient() *http.Client {
	client := &http.Client{
		Timeout: 60 * time.Second,
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 10,
			Dial: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 5 * time.Second,
		},
	}

	return client
}

// NewClient returns an instantiated BaseClient using the provided information
func NewClient(host string, httpc *http.Client, auth AuthProvider) *BaseClient {
	if httpc == nil {
		httpc = newHTTPClient()
	}
	return &BaseClient{
		HTTPClient: httpc,
		Auth:       auth,
		Host:       host,
	}
}
