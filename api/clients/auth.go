package clients

import (
	"net/http"
)

// AuthProvider provides a way to sign outbound HTTP requests
type AuthProvider interface {
	SetAuth(*http.Request)
}
