package giphy

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"bitbucket.org/hussamd/imagefav/api/clients"
)

const (
	pathSearch = "/gifs/search"
	pathGet    = "/gifs"
)

const (
	_ = iota
	// v1 is the latest available client for the Giphy API
	v1 = iota
	// DefaultVer is the default version of the Giphy API client used by us
	DefaultVer = iota - 1
)

// execWithContext calls the given client's HTTPDo method and unmarshals
// any response into the provided `dest` object.
//
// Using this method ensures that any cancellations upstream will result in
// the link closing immediately avoiding unnecessary cycles.
func execWithContext(
	ctx context.Context, c *DefaultClient, req *http.Request, data interface{},
) error {
	type grouped struct {
		r   *http.Response
		err error
	}
	comm := make(chan grouped, 1)

	go func() {
		resp, err := c.HTTPDo(
			req, []int{http.StatusOK}, func(b io.ReadCloser) error {
				err := json.NewDecoder(b).Decode(data)
				return err
			},
		)
		comm <- grouped{resp, err}
	}()

	select {
	case <-ctx.Done():
		<-comm
		return ctx.Err()
	case grouped := <-comm:
		if grouped.err != nil {
			return grouped.err
		}
		return nil
	}
}

// DefaultClient is a http client for the Giphy API that implements Client
type DefaultClient struct {
	*clients.BaseClient
}

// BasePath implements the GiphyAPI interface
func (c *DefaultClient) BasePath() string {
	return fmt.Sprintf("/v%d", DefaultVer)
}

// Get retrieves a single gif by it's id
func (c *DefaultClient) Get(ctx context.Context, id string) (GetResp, error) {
	uri := c.BasePath() + pathGet + fmt.Sprintf("/%s", id)
	params := map[string]string{
		"fmt": "json",
	}
	req, err := c.BuildRequest(ctx, http.MethodGet, uri, nil, params, nil)
	if err != nil {
		return GetResp{}, err
	}

	// Execute
	var data GetResp
	if err := execWithContext(ctx, c, req, &data); err != nil {
		return GetResp{}, err
	}

	return data, nil
}

// Search uses the term provided to find Gifs and returns a subset of the results
func (c *DefaultClient) Search(
	ctx context.Context, value, rating string, limit, offset int,
) (SearchResp, error) {
	uri := c.BasePath() + pathSearch
	params := map[string]string{
		"q":      value,
		"rating": rating,
		"limit":  strconv.Itoa(limit),
		"offset": strconv.Itoa(offset),
		"fmt":    "json",
	}
	req, err := c.BuildRequest(ctx, http.MethodGet, uri, nil, params, nil)
	if err != nil {
		return SearchResp{}, err
	}

	// Execute
	var data SearchResp
	if err := execWithContext(ctx, c, req, &data); err != nil {
		return SearchResp{}, err
	}

	return data, nil
}
