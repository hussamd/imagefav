package giphy

import (
	"context"
	"net/http"
)

// Client defines all methods that a Giphy client should support
type Client interface {
	BasePath() string
	Get(context.Context, string) (GetResp, error)
	Search(context.Context, string, string, int, int) (SearchResp, error)
}

// Still is a snapshot of a gif
type Still struct {
	URL string `json:"url"`
}

// Images holds details about the graphical portion of a single Gif
type Images struct {
	Preview  ImageRecord `json:"fixed_width"`
	Original ImageRecord `json:"original"`
	Still    Still       `json:"fixed_width_still"`
}

// Dimensions contains the height and width of a gif
type Dimensions struct {
	W string `json:"width"`
	H string `json:"height"`
}

// ImageRecord is an item within the images portion of a single Gif
type ImageRecord struct {
	URL  string `json:"url"`
	Size string `json:"size"`
	Dimensions
}

// Gif is a single image available on the Giphy website
type Gif struct {
	ID    string `json:"id"`
	Title string `json:"title"`
	URL   string `json:"url"`
	Links Images `json:"images"`
}

// Gifs is a slice of Gif
type Gifs []Gif

type searchRespPagination struct {
	Total int `json:"total_count"`
}

// GetResp is used to populate the results of fetching a single Gif
type GetResp struct {
	Gif Gif `json:"data"`
}

// SearchResp is used to populate the results of performing a query
type SearchResp struct {
	Gifs       Gifs                 `json:"data"`
	Pagination searchRespPagination `json:"pagination"`
}

// Auth provides a way to authenticate outbound requests to Giphy API
type Auth struct {
	Key string
}

// SetAuth implements the AuthProvider interface
func (a Auth) SetAuth(r *http.Request) {
	q := r.URL.Query()
	q.Add(paramAPIKey, a.Key)
	r.URL.RawQuery = q.Encode()
}
