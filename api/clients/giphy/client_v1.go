package giphy

// ClientV1 is a http client for v1 of the Giphy API and implements GiphyAPI
type ClientV1 struct {
	*DefaultClient
}
