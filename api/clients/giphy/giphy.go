package giphy

import (
	"net/http"

	"bitbucket.org/hussamd/imagefav/api/clients"
)

const (
	paramAPIKey = "api_key"
)

// NewClient instantiates a new BaseClient for use with the Giphy API
func NewClient(host, apiKey string, ver int, httpc *http.Client) Client {
	baseClient := clients.NewClient(host, httpc, Auth{Key: apiKey})
	switch ver {
	case v1:
		return &ClientV1{
			DefaultClient: &DefaultClient{
				BaseClient: baseClient,
			},
		}
	default:
		return &DefaultClient{
			BaseClient: baseClient,
		}
	}
}
