package handlers

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Handle is a method signature that extends httprouter.Handle by adding an
// error to the response
type Handle func(http.ResponseWriter, *http.Request, httprouter.Params) error
