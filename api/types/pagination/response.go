package pagination

import (
	"log"
	"net/url"
	"strconv"
)

// Link holds a URL to a given page
type Link struct {
	Href string `json:"href"`
}

// Links contains pagination information relevant to the current page being
// viewed, with links to other pages needed for navigation
type Links struct {
	Self  *Link `json:"self"`
	First *Link `json:"first"`
	Prev  *Link `json:"prev,omitempty"`
	Next  *Link `json:"next,omitempty"`
	Last  *Link `json:"last"`
}

// NewLinks generates pagination links based on the current page's parameters
func NewLinks(url string, total int, page Page, params url.Values) Links {
	prevOffset := page.Offset - page.Limit
	nextOffset := page.Offset + page.Limit

	l := Links{
		Self:  newLink(url, page.Limit, page.Offset, params),
		First: newLink(url, page.Limit, 0, params),
		Prev:  newLink(url, page.Limit, prevOffset, params),
		Next:  newLink(url, page.Limit, nextOffset, params),
		Last:  newLink(url, page.Limit, total-page.Limit, params),
	}

	if nextOffset >= total {
		l.Next = nil
	}
	if page.Offset == 0 || page.Offset < page.Limit {
		l.Prev = nil
	}
	if total == 0 {
		l.Last = l.First
	}
	return l
}

func newLink(absURL string, limit, offset int, params url.Values) *Link {
	u, err := url.Parse(absURL)
	if err != nil {
		log.Panic(err)
	}

	q := u.Query()
	// Copy existing params
	for paramName, paramValues := range params {
		for _, val := range paramValues {
			q.Add(paramName, val)
		}
	}
	// Overwrite pagination params
	q.Set("limit", strconv.Itoa(limit))
	q.Set("offset", strconv.Itoa(offset))

	u.RawQuery = q.Encode()
	return &Link{Href: u.String()}
}
