package pagination

import (
	"net/url"
	"strconv"

	"bitbucket.org/hussamd/imagefav/api/errors"
	"bitbucket.org/hussamd/imagefav/setup"
)

// Page is a way to limit the number of results on a page and to indicate
// the starting record of a given page
type Page struct {
	Limit  int
	Offset int
}

// FromParams returns a Page object by inspecting the query params sent
// to a given route.  If no pagination params are found, defaults are returned.
func FromParams(params url.Values) (Page, error) {
	pageCfg := setup.AppConfig.Pagination
	limit, offset := pageCfg.DefaultLimit, pageCfg.DefaultOffset
	p := Page{limit, offset}

	if params.Get("limit") != "" {
		l, err := strconv.Atoi(params.Get("limit"))
		if err != nil {
			return p, errors.ErrInvalidLimit
		}
		limit = l
	}
	if 0 > limit || limit > pageCfg.MaxLimit {
		return p, errors.ErrInvalidLimit
	}

	if params.Get("offset") != "" {
		o, err := strconv.Atoi(params.Get("offset"))
		if err != nil {
			return p, errors.ErrInvalidOffset
		}
		offset = o
	}
	if 0 > offset {
		return p, errors.ErrInvalidOffset
	}

	p.Limit, p.Offset = limit, offset
	return p, nil
}
