// Package errors contains all user facing and internal errors that can be
// used by the API layer of this app.
package errors

import (
	"fmt"
	"net/http"
)

// HTTPError is the interface that provides a Status method.
//
// Status returns the HTTP status code representing a given error.
type HTTPError interface {
	Status() int
	Error() string
}

// NotFound denotes a failure in finding a resource
type NotFound struct {
	Message string
}

// Status returns the HTTP status code for a not found error
func (n NotFound) Status() int {
	return http.StatusNotFound
}

// Error returns the string representation of NotFound
func (n NotFound) Error() string {
	return n.Message
}

// InternalServer denotes a failure in request handling
type InternalServer struct {
	Message string
}

// Status returns the HTTP status code for an internal server error
func (i InternalServer) Status() int {
	return http.StatusInternalServerError
}

// Error returns the string representation of InternalServer
func (i InternalServer) Error() string {
	return i.Message
}

// BadRequest denotes that an invalid request was made
type BadRequest struct {
	Message string
}

// Status returns the HTTP status code for a bad request
func (b BadRequest) Status() int {
	return http.StatusBadRequest
}

// Error returns the string representation of BadRequest
func (b BadRequest) Error() string {
	return b.Message
}

// Forbidden denotes that the action being performed is not allowed
type Forbidden struct {
	Message string
}

// Status returns the HTTP status code for a forbidden request
func (f Forbidden) Status() int {
	return http.StatusForbidden
}

// Error returns the string representation of Forbidden
func (f Forbidden) Error() string {
	return f.Message
}

// Unauthorized denotes that authentication is missing or invalid
type Unauthorized struct {
	Message string
}

// Status returns the HTTP status code for an unauthorized request
func (u Unauthorized) Status() int {
	return http.StatusUnauthorized
}

// Error returns the string representation of Unauthorized
func (u Unauthorized) Error() string {
	return u.Message
}

// Conflict denotes that the action being performed conflicts with an existing resource
type Conflict struct {
	Message string
}

// Status returns the HTTP status code for a conflict error
func (c Conflict) Status() int {
	return http.StatusUnauthorized
}

// Error returns the string representation of Conflict
func (c Conflict) Error() string {
	return c.Message
}

// NewBadRequestError returns a new BadRequest type with the provided message
func NewBadRequestError(m string) BadRequest {
	return BadRequest{Message: m}
}

// NewUnauthorizedError returns a new Unauthorized type with the provided message
func NewUnauthorizedError(m string) Unauthorized {
	return Unauthorized{Message: m}
}

// NewConflictError returns a new Conflict type with the provided message
func NewConflictError(m string) Conflict {
	return Conflict{Message: m}
}

// NewForbiddenError returns a new Forbidden type with the provided message
func NewForbiddenError(m string) Forbidden {
	return Forbidden{Message: m}
}

// NewFieldError returns a new BadRequest type with a message about the provided field
func NewFieldError(field string, err error) BadRequest {
	m := fmt.Sprintf("Provided value for %s is invalid: %s", field, err.Error())
	return BadRequest{Message: m}
}

// NewInternalServerError returns a new InternalServer type with the provided message
func NewInternalServerError(m string) InternalServer {
	return InternalServer{Message: m}
}

// NewNotFoundError returns a new NotFound type with the provided message
func NewNotFoundError(m string) NotFound {
	return NotFound{Message: m}
}

// NewMarshalingError returns an InternalServer with a reason for the error
func NewMarshalingError(err error) InternalServer {
	return InternalServer{fmt.Sprintf("Could not marshal results: %v\n", err)}
}

// NewResponseError returns an InternalServer with a reason for the error
func NewResponseError(err error) InternalServer {
	return InternalServer{fmt.Sprintf("Could not write to response writer: %v\n", err)}
}

var (
	// ErrInvalidSearchParam is used when a required query parameter is missing or invalid
	ErrInvalidSearchParam = NewBadRequestError("You have not sent a proper search query param")
	// ErrInvalidLimit is used when the query paramer value for limit is invalid
	ErrInvalidLimit = NewBadRequestError("Your value for `limit` is invalid")
	// ErrInvalidOffset is used when the query parameter value for offset is invalid
	ErrInvalidOffset = NewBadRequestError("Your value for `offset` is invalid")
	// ErrEmptyBody is used when an expected payload was not sent
	ErrEmptyBody = NewBadRequestError("Your request is missing a proper body")
	// ErrInvalidBody is used when an expected payload doesn't match a schema
	ErrInvalidBody = NewBadRequestError("Your request body is invalid")
	// ErrMissingAuth is used when authorization information is missing
	ErrMissingAuth = NewUnauthorizedError("You request is missing auth details")
	// ErrInvalidAuth is used when authorization is invalid
	ErrInvalidAuth = NewUnauthorizedError("Invalid auth")
	// ErrFailedAuth is used when an authorization attempt fails
	ErrFailedAuth = NewUnauthorizedError("Authorization failed")
	// ErrInvalidRegistration is used when a registration attempt is invalid
	ErrInvalidRegistration = NewBadRequestError("Invalid registration details")
	// ErrInvalidSession is used when a session id does not exist
	ErrInvalidSession = NewUnauthorizedError("Session does not exist")
	// ErrUsernameTaken is used when a registered user previously exists with the given username
	ErrUsernameTaken = NewConflictError("Username is taken")
	// ErrInvalidEmail is used when an email doesn't pass a proper validation check
	ErrInvalidEmail = NewBadRequestError("Your email address is invalid")
	// ErrNameTooShort is used when a user provides an exceptionally short name
	ErrNameTooShort = NewBadRequestError("Your name cannot be pronounced")
	// ErrNameless is used when a user does not provide a name
	ErrNameless = NewBadRequestError("You cannot be nameless")
	// ErrNameTooLong is used when a user provides an exceptionally long name
	ErrNameTooLong = NewBadRequestError("That is not your name")
	// ErrForbidden is used when a user performs an unauthenticated action
	ErrForbidden = NewForbiddenError("Operation not allowed")
	// ErrSessionExpired is used when a user's session has expired
	ErrSessionExpired = NewUnauthorizedError("Session expired")
	// ErrInvalidImage is used when an invalid image id is being favorited
	ErrInvalidImage = NewBadRequestError("Invalid image id")
	// ErrImageNotFound is used when an image is not found on the Giphy website
	ErrImageNotFound = NewNotFoundError("Image not found")
	// ErrInvalidQuery is used when a query for resources is invalid
	ErrInvalidQuery = NewBadRequestError("Invalid query")
	// ErrInvalidTag is used when a tag provided for an image is invalid
	ErrInvalidTag = NewBadRequestError("Invalid tag")
)
