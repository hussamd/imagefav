package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/hussamd/imagefav/api/server/routes"
	"bitbucket.org/hussamd/imagefav/setup"
)

func main() {
	setup.InitConfig()
	if setup.AppConfig.Error != nil {
		log.Fatalf("Cannot start app. Exiting!")
	}

	addr := fmt.Sprintf(":%s", setup.AppConfig.Port)
	log.Printf("Starting server on: %s", addr)
	log.Fatal(http.ListenAndServe(addr, routes.Bootstrap()))
}
