# ImageFav

A demo app in Go for favoriting images

---

### Demo and development setup
1. Install Docker as described [here](https://docs.docker.com/v17.09/docker-for-mac/install/)

2. Set up the following environment variables:

         # These variables must be exported for the app to function properly
         export DB_PASSWORD=3D#%dUfd
         export GIPHY_PASSWORD=WRVYNgGpkLdBqgH669KrsHagC6H59Y9R
         export GO111MODULE=on

3. You can also create an alias in your shell rc file:

         imagefav_docker_exports() {
          // exports go here
         }

4. Verify your docker-compose setup:

         docker-compose config
         # Ensure all variables under `environment:` have a value and are not blank or `null`

5. Run the containers:

         docker-compose up
         # Look for the db to become ready: ...init process complete; ready for start up...

6. Verify you are able to talk to the app:

        curl -sk 'http://localhost:8080/search?q=homer' | jq -r '.'
