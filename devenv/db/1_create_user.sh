#!/bin/bash

{
psql --user postgres \
    --set db="$DB_NAME" \
    --set pass="${DB_PASSWORD}" \
    --set user="$DB_USER" \
    --set enc="$DB_ENCODING" <<-'EOSQL'
    CREATE ROLE :user with LOGIN;
    ALTER USER :user WITH PASSWORD :'pass';
EOSQL
}
