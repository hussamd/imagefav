-- users
CREATE TABLE users (
    id serial  NOT NULL,
    username varchar(20)  NOT NULL,
    password varchar(255)  NOT NULL,
    name varchar(255)  NOT NULL,
    email varchar(255)  NOT NULL,
    CONSTRAINT users_username_unique UNIQUE (username),
    CONSTRAINT users_pk PRIMARY KEY (id)
);

CREATE INDEX users_idx_1 on users (username ASC);

-- user_favorites
CREATE TABLE user_favorites (
    id serial  NOT NULL,
    user_id int  NOT NULL,
    image_id varchar(25)  NULL,
    url varchar(255) NOT NULL,
    tags varchar(25)[] DEFAULT '{}',
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT user_favorites_pk PRIMARY KEY (id),
    CONSTRAINT user_favorites_uniq_image_user_id UNIQUE (image_id, user_id)
);

CREATE INDEX user_favorites_user_id_idx on user_favorites (user_id ASC);
CREATE INDEX user_favorites_tags_idx on user_favorites USING GIN(tags);

-- foreign keys
ALTER TABLE user_favorites ADD CONSTRAINT user_favorites_users
    FOREIGN KEY (user_id)
    REFERENCES users (id)
    ON DELETE  CASCADE
;

-- GRANT ALL PRIVILEGES ON imgfav TO imgfav;
GRANT CONNECT ON DATABASE imgfav TO imgfav;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO imgfav;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO imgfav;
