module bitbucket.org/hussamd/imagefav

require (
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/julienschmidt/httprouter v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.1.1
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.28.0
)
