package redis

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/hussamd/imagefav/setup"
	"github.com/go-redis/redis"
)

// SimpleClient is an interface for redis that performs a subset of all
// supported operations by a redis server
type SimpleClient interface {
	Get(string) (string, error)
	Set(string, interface{}, time.Duration) error
	Del(string) error
	TTL(string) (time.Duration, error)
	Expire(string, time.Duration) error
	GetUnmarshaled(string, interface{}) error
}

// Client is an implementation of the SimpleClient interface
type Client struct {
	redisClient *redis.Client
}

// ErrNotFound is used when a key does not exist
var ErrNotFound = fmt.Errorf("Key not found")

// Del deletes any values for the given key.
//
//
// It does not raise an error if the key does not exist.
func (r Client) Del(key string) error {
	if _, err := r.redisClient.Del(key).Result(); err != redis.Nil && err != nil {
		return err
	}
	return nil
}

// Get returns the value for key if it exists, or an empty string otherwise.
//
// Use this when you know that the value stored is a string value. Otherwise,
// use GetUmnarshaled with a typed object to unmarshal into.
func (r Client) Get(key string) (string, error) {
	val, err := r.redisClient.Get(key).Result()
	if err != redis.Nil && err != nil {
		return "", err
	}

	return val, nil
}

// GetUnmarshaled returns a json decoded version of the data stored at `key`
func (r Client) GetUnmarshaled(key string, data interface{}) error {
	val, err := r.redisClient.Get(key).Result()
	if err != redis.Nil && err != nil {
		return err
	}

	if err != redis.Nil {
		if err := json.Unmarshal([]byte(val), data); err != nil {
			return err
		}
		return nil
	}

	return ErrNotFound
}

// TTL returns the remaining expiration time on a given key.
//
// This can return an error if there was no expiration set on the key itself.
func (r Client) TTL(key string) (time.Duration, error) {
	val, err := r.redisClient.TTL(key).Result()
	if err != redis.Nil && err != nil {
		return val, err
	}

	return val, nil
}

// Set stores the given value for key
func (r Client) Set(key string, val interface{}, timeout time.Duration) error {
	bs, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Failed marshaling of data for redis: %v", err)
	}
	return r.redisClient.Set(key, bs, timeout).Err()
}

// Expire sets a ttl on the give key
func (r Client) Expire(key string, ttl time.Duration) error {
	_, err := r.redisClient.Expire(key, ttl).Result()
	if err != redis.Nil && err != nil {
		return err
	}
	return nil
}

// NewClient returns a new connection to the redis server
func NewClient() (*Client, error) {
	appCfg := setup.AppConfig
	client := redis.NewClient(&redis.Options{
		Addr: appCfg.RedisURL,
		DB:   0, // default db
	})

	if _, err := client.Ping().Result(); err != nil {
		return nil, err
	}

	return &Client{redisClient: client}, nil
}
