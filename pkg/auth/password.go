package auth

import (
	"golang.org/x/crypto/bcrypt"
)

// https://medium.com/@jcox250/password-hash-salt-using-golang-b041dc94cb72

// Password holds a secret that can be printed but not retrieved
type Password struct {
	value []byte
	Hash  []byte
}

// NewPassword returns a new instance of Password with the provided value
func NewPassword(password, hash []byte) *Password {
	return &Password{[]byte(password), hash}
}

// NewHash calculates and returns a salted hash for the value stored in Password
func (p Password) NewHash() (string, error) {
	if p.Hash != nil {
		return string(p.Hash), nil
	}

	hash, err := bcrypt.GenerateFromPassword(p.value, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	p.Hash = hash
	return string(p.Hash), nil
}

// Compare returns whether the current Password and the given plain text
// password match by verifying their hashes
func (p Password) Compare(password string) (bool, error) {
	hash, err := p.NewHash()
	if err != nil {
		return false, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, err
	}

	return true, nil
}
