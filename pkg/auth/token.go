package auth

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/hex"
)

// NewToken generates a new HMAC using a SHA256 hash type encrypted with `key`
func NewToken(secret, data []byte) string {
	h := hmac.New(sha256.New, secret)
	h.Write(data)
	return hex.EncodeToString(h.Sum(nil))
}

// Compare checks the given tokens to be equal using a constant time comparison
// method to avoid timing attacks
func Compare(t1, t2 []byte) bool {
	return subtle.ConstantTimeCompare(t1, t2) == 1 // 1 means equal, 0 otherwise
}
