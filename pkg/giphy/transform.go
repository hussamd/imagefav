package giphy

import (
	"bitbucket.org/hussamd/imagefav/api/clients/giphy"
)

// Dimensions holds information about the width and height of an image or video
type Dimensions struct {
	W string `json:"width"`
	H string `json:"height"`
}

// ImageRecord holds size/location/dimensional information about an image
type ImageRecord struct {
	URL  string `json:"url"`
	Size string `json:"size"`
	Dimensions
}

// Image points to the location of a still taken from a video
type Image struct {
	URL string `json:"url"`
}

// Links holds preview related information about an image
type Links struct {
	Preview  ImageRecord `json:"preview"`
	Original ImageRecord `json:"original"`
	Still    Image       `json:"still"`
}

// Gif holds links and IDs for a single image hosted on the Giphy website
type Gif struct {
	ID    string `json:"id"`
	Title string `json:"title"`
	URL   string `json:"url"`
	Links Links  `json:"links"`
}

// TransformResults converts the SearchResp from giphy.clients.Search to
// a Gif type
func TransformResults(res giphy.SearchResp) []Gif {
	gifs := make([]Gif, len(res.Gifs))
	for i, gif := range res.Gifs {
		gifs[i] = Gif{
			ID:    gif.ID,
			Title: gif.Title,
			URL:   gif.URL,
		}

		gifs[i].Links.Preview = copyImageRecord(gif.Links.Preview)
		gifs[i].Links.Original = copyImageRecord(gif.Links.Original)
		gifs[i].Links.Still = Image(gif.Links.Still)
	}
	return gifs
}

func copyImageRecord(src giphy.ImageRecord) ImageRecord {
	return ImageRecord{
		URL:  src.URL,
		Size: src.Size,
		Dimensions: Dimensions{
			W: src.Dimensions.H,
			H: src.Dimensions.W,
		},
	}
}
