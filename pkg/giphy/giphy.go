package giphy

import (
	"context"

	"bitbucket.org/hussamd/imagefav/api/clients/giphy"
	"bitbucket.org/hussamd/imagefav/api/types/pagination"
	"bitbucket.org/hussamd/imagefav/setup"
)

const (
	// This service can only serve a G rating image
	defaultRating = "g"
)

// NewClient is a method to instantiate a Giphy API client
var NewClient = giphy.NewClient

// Get returns a single image object from the giphy API
func Get(ctx context.Context, imageID string) (giphy.GetResp, error) {
	cfg := setup.AppConfig
	client := NewClient(cfg.GiphyURL, cfg.GiphyPassword, giphy.DefaultVer, nil)
	return client.Get(ctx, imageID)
}

// Search queries the giphy API for `term`
func Search(ctx context.Context, term string, page pagination.Page) (giphy.SearchResp, error) {
	cfg := setup.AppConfig
	client := NewClient(cfg.GiphyURL, cfg.GiphyPassword, giphy.DefaultVer, nil)
	return client.Search(ctx, term, defaultRating, page.Limit, page.Offset)
}
