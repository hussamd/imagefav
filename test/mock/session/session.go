package session

import (
	"fmt"
	"time"

	"bitbucket.org/hussamd/imagefav/pkg/redis"
)

// NewTestClient returns a TestSimpleClient object
var NewTestClient = func(shouldError bool, ttl time.Duration, sess map[string]string) func() redis.SimpleClient {
	return func() redis.SimpleClient {
		return TestClient{shouldError, ttl, sess}
	}
}

// TestInterface is a mock interface to satisfy the requirements of redis.SimpleClient
type TestInterface interface {
	Get(string) (string, error)
	Set(string, interface{}, time.Duration) error
	Del(string) error
	TTL(string) (time.Duration, error)
	Expire(string, time.Duration) error
	GetUnmarshaled(string, interface{}) error
}

// TestClient is an implmenetation of TestInterface/redis.SimpleClient and
// is used by tests.
type TestClient struct {
	shouldError bool
	ttl         time.Duration
	sess        map[string]string
}

// Get is a mock method for retrieving information from a backend
func (t TestClient) Get(key string) (string, error) {
	if t.shouldError {
		return key, fmt.Errorf("TestClient.Get(%s) was told the error", key)
	}
	return key, nil
}

// GetUnmarshaled is a mock method for retrieving unmarshaled data from a backend
func (t TestClient) GetUnmarshaled(key string, dest interface{}) error {
	if t.shouldError {
		return fmt.Errorf("TestClient.GetUnmarshaled(%s) was told to error", key)
	}

	d, _ := dest.(*map[string]string)
	if t.sess != nil {
		(*d)["id"] = t.sess["id"]
		(*d)["username"] = t.sess["username"]
		(*d)["expires"] = t.sess["expires"]
	}
	return nil
}

// TTL is a mock method for retrieving the expiration time set on a key
func (t TestClient) TTL(key string) (time.Duration, error) {
	if t.shouldError {
		return t.ttl, fmt.Errorf("TestClient.TTL(%s) was told to error", key)
	}
	return t.ttl, nil
}

// Expire is a mock method to set the expiration time on a key
func (t TestClient) Expire(key string, exp time.Duration) error {
	if t.shouldError {
		return fmt.Errorf("TestClient.Expire(%s) was told to error", key)
	}
	return nil
}

// Set is a mock method for storing information in a backend
func (t TestClient) Set(key string, data interface{}, exp time.Duration) error {
	if t.shouldError {
		return fmt.Errorf("TestClient.Set(%s) was told to error", key)
	}
	fmt.Printf("TestClient.Set(%s): Would have stored %+v with timeout: %v\n", key, data, exp)
	return nil
}

// Del is a mock method for removing information from a backend
func (t TestClient) Del(key string) error {
	if t.shouldError {
		return fmt.Errorf("TestClient.Del(%s) was told to error", key)
	}
	fmt.Printf("TestClient.Del(%s): Would have deleted a key\n", key)
	return nil
}
