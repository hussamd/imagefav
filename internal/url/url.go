package url

import (
	"log"
	"net/url"
)

// AbsURL builds an abs URL given a base host and a relative uri
func AbsURL(host, uri string) string {
	partial, err := url.Parse(uri)
	if err != nil {
		log.Panic(err)
	}

	base, err := url.Parse(host)
	if err != nil {
		log.Panic(err)
	}

	return base.ResolveReference(partial).String()
}
