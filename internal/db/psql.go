package db

import (
	"context"
	"database/sql"
	"fmt"
	// This registers the postgres drivers with the database/sql package.
	// It is not directly used.
	_ "github.com/lib/pq"
	"log"
	"sync"
	"time"

	"bitbucket.org/hussamd/imagefav/setup"
)

var pool *sql.DB
var once sync.Once

func init() {
	setup.InitConfig()
	appCfg := setup.AppConfig

	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		appCfg.DBHost, appCfg.DBPort, appCfg.DBUser, appCfg.DBPassword, appCfg.DBName,
	)

	once.Do(func() {
		db, err := sql.Open("postgres", dsn)
		mustNot(err)

		err = db.Ping()
		mustNot(err)

		pool = db
		pool.SetMaxOpenConns(appCfg.DBMaxOpenConn)
		pool.SetMaxIdleConns(appCfg.DBMaxIdleConn)
		pool.SetConnMaxLifetime(time.Duration(appCfg.DBMaxLifeConn) * time.Minute)

		log.Println("DB initialized")
	})
}

func mustNot(err error) {
	if err != nil {
		log.Panicf("Database operation failed: %v", err)
	}
}

// Connect returns a single connection to the DB from a pool
func Connect(ctx context.Context) (*sql.Conn, error) {
	return pool.Conn(ctx)
}
